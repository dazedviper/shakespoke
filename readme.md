shakespoke
==========

An HTTP API that returns a Pokémon's description in Shakespearean English,
implmented in [Rust].

[Rust]: https://www.rust-lang.org/

Build and run
-------------

### With `cargo`

Install a working Rust toolchain using [rustup]. Run the binary crate as
follows:

```sh
➜ RUST_LOG=debug cargo run --release
```

To run the tests:

```sh
➜ cargo run test
```

To run the application's single integration test:

```sh
➜ cargo run test -- --ignored
```

[rustup]: https://rustup.rs/

### With Docker

```sh
➜ docker build .
...
Successfully built 52d3558a01c2
➜ docker docker run -p 8080:8080 52d3558a01c2
```

Usage
-----

### As an HTTP API

Run the binary crate, which exposes the API at port `8080`, and hit the route
`/pokemon/{name}`.

```sh
➜ http localhost:8080/pokemon/magikarp
HTTP/1.1 200 OK
content-length: 146
content-type: application/json
date: Mon, 08 Mar 2021 00:30:21 GMT

{
    "description": "In the distant past,  't wast somewhat stronger than the horribly weak descendants yond exist the present day.",
    "name": "magikarp"
}
```

The example above uses [HTTPie] to call the API.

[HTTPie]: https://httpie.io/

### As a library

```rust
use shakespoke::get_description;

let shakespeare_description = get_description("magikarp").await.expect("shakespoke error");
```

You can generate the library's documentation running `cargo doc`.

How it works
------------

The first Pokémon's English description is fetched from [PokéAPI] using
[pokerust] bindings. The description is then translated into Shakespeare's
style using [this Shakespeare translator API].

[PokéAPI]: https://pokeapi.co/
[pokerust]: https://lib.rs/crates/pokerust
[this Shakespeare translator API]: https://funtranslations.com/api/shakespeare

Things I would have done better/differently
-------------------------------------------

* `pokerust` uses `minreq` to hit PokéAPI, which is blocking. For the purposes
  of this task, it would have been fine to write minimal code to parse the
  responses from the `/pokemon-species` endpoint and ditch `pokerust`.
    - An upstream contribution to `pokerust` including support for using an
      `async` client would have been ideal.
    - Spinning a thread pool and allocating blocking tasks to threads would
      have also been fine.
* The main benefit of using `pokerust` and the reason why I chose it over
  implementing a client myself is that it memoizes responses from the PokéAPI
  using the [cached](https://lib.rs/crates/cached) crate. However, a
  disadvantage of `pokerust` is that it exposes all errors as `minreq::Error`
  after deserializing the JSON response. This means that we cannot distinguish
  an unexpected JSON parsing error from, say, a 404 indicating the Pokémon
  we've requested was not found. We cannot therefore surface these kind of
  errors to our end users, limiting ourselves to returning "Internal Server
  Error"s when they happen.
* In `shakespeare.rs`, we use `reqwest`. We could dispense with `reqwest` and
  use Actix's builtin HTTP client, but that would tie the Shakespeare module to
  Actix's runtime. Ideally it should be a *separate crate* that can be used by
  other applications, not only our web application. For simplicity, I have
  chosen to simply put it in a separate module to somewhat convey this design
  consideration. For the purposes of this task, however, where we don't foresee
  anyone else to use the Shakespeare module but our Actix web application, it
  would be leaner/less complex to just put everything in the same module and
  tie it to Actix's runtime.
   - A performance disadvantage of having the `shakespeare` module be
     completely agnostic of our binary application is that it will be
     initiating a brand new HTTPS connection to the Shakespeare API for
     every call to `translate`, which can be very wasteful. See [this blog
     post] for details. If the module were tightly integrated into our web
     application, we could make it so that it reuses the same connection
     across different requests to our HTTP API.
* A production service ought to have metrics and off-host logging. While I've
  logged stuff to `stdout`, I haven't done metrics at all, since it would be
  too dependent on the mechanism used to push (or pull) metrics and logs.

[this blog post]: https://www.lpalmieri.com/posts/how-to-write-a-rest-client-in-rust-with-reqwest-and-wiremock/#how-to-reuse-the-same-reqwestclient-in-actix-web
