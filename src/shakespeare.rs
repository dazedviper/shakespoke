use log::error;
use maplit::hashmap;
use reqwest::Client;
use thiserror::Error;

use serde::Deserialize;

const HOST_URI: &str = "https://api.funtranslations.com";
const PATH_URI: &str = "/translate/shakespeare.json";

#[derive(Error, Debug, PartialEq)]
pub enum Error {
    #[error("Error hitting Shakespeare translator API")]
    UnexpectedError,

    #[error("Quota to Shakespeare translator API exceeded")]
    QuotaError,

    #[error("Error parsing response from Shakespeare translator API")]
    ParseJson(String),
}

#[derive(Deserialize, Debug)]
struct Translation {
    success: Success,
    contents: Contents,
}

#[derive(Deserialize, Debug)]
struct Success {
    total: u32,
}

#[derive(Deserialize, Debug)]
struct Contents {
    translated: String,
}

pub type ShakespeareResult = Result<String, Error>;

impl Translation {
    fn translated_text(&self) -> ShakespeareResult {
        match self.success.total {
            1 => Ok(self.contents.translated.clone()),
            _ => Err(Error::UnexpectedError),
        }
    }
}

pub async fn translate(text: &str) -> ShakespeareResult {
    translate_inner(text, &format!("{}{}", HOST_URI, PATH_URI)).await
}

async fn translate_inner(text: &str, url: &str) -> ShakespeareResult {
    let json_param = hashmap! { "text" => text };

    let response = Client::new()
        .post(url)
        .json(&json_param)
        .send()
        .await
        .map_err(|e| {
            error!("Shakespeare API request failed: {}", e);
            Error::UnexpectedError
        })?;

    match response.status().as_u16() {
        429 => Err(Error::QuotaError),
        200 => {
            let translation: Translation = response.json().await.map_err(|e| {
                error!(
                    "Shakespeare API parse JSON payload error: text: {}, error: {}",
                    text, e
                );
                Error::ParseJson(e.to_string())
            })?;
            translation.translated_text()
        }
        _ => {
            error!(
                "Shakespeare API unexpected status code. text: {}, response: {:#?}",
                text, response
            );
            Err(Error::UnexpectedError)
        }
    }
}

#[cfg(test)]
mod tests {
    use httpmock::{Method::POST, MockServer};
    use serde_json::json;

    use super::*;

    /// Helper function to generate URLs to the Shakespeare API endpoint.
    fn format_url(host: &std::net::SocketAddr) -> String {
        format!("http://{}{}", host, PATH_URI)
    }

    #[tokio::test]
    async fn successful_translation() {
        let text = "If you read this bring it up in the interview";
        let expected_translation = "To see if you read my code";
        let expected_response = json!({
            "success": {
                "total": 1
            },
            "contents": {
                "translated": expected_translation,
                "text": text,
                "translation": "shakespeare"
            }
        });

        let server = MockServer::start();

        let mock = server.mock(|when, then| {
            when.method(POST)
                .path(PATH_URI)
                .header("Content-Type", "application/json")
                .json_body(json!({ "text": text }));
            then.status(200)
                .header("Content-Type", "application/json")
                .json_body(expected_response);
        });

        let translation = translate_inner(text, &format_url(server.address())).await;

        mock.assert();
        assert_eq!(Ok(String::from(expected_translation)), translation);
    }

    #[tokio::test]
    async fn unsuccessful_translation_bad_response() {
        let server = MockServer::start();

        let mock = server.mock(|when, then| {
            when.method(POST).path(PATH_URI);
            then.status(500);
        });

        let translation = translate_inner("Save my soul", &format_url(server.address())).await;

        mock.assert();
        assert_eq!(Err(Error::UnexpectedError), translation);
    }

    #[tokio::test]
    async fn unsuccessful_translation_parse_json_error() {
        let server = MockServer::start();

        let mock = server.mock(|when, then| {
            when.method(POST).path(PATH_URI);
            then.status(200)
                .header("Content-Type", "application/json")
                .json_body(json!({"not what we": "expected"}));
        });

        let translation = translate_inner(
            "If you're happy and you know it clap your hands",
            &format_url(server.address()),
        )
        .await;

        mock.assert();
        assert!(std::matches!(translation, Err(Error::ParseJson(_))));
    }

    #[tokio::test]
    async fn unsuccessful_translation_quota_exceeded() {
        let server = MockServer::start();

        let mock = server.mock(|when, then| {
            when.method(POST).path(PATH_URI);
            then.status(429);
        });

        let translation = translate_inner("What is love", &format_url(server.address())).await;

        mock.assert();
        assert_eq!(Err(Error::QuotaError), translation);
    }
}
