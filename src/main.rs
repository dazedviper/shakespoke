#![forbid(unsafe_code)]

use crate::errors::ServiceError;
use actix_web::{get, web, App, HttpResponse, HttpServer};
use log::info;
use serde::{Deserialize, Serialize};
use shakespoke::get_description;

mod errors;

#[derive(Debug, Serialize, Deserialize, PartialEq)]
struct Pokemon {
    name: String,
    description: String,
}

#[get("/pokemon/{pokemon_name}")]
async fn get_pokemon_shakespeare_description(
    web::Path(pokemon_name): web::Path<String>,
) -> Result<HttpResponse, ServiceError> {
    info!("/pokemon/{}", pokemon_name);

    let shakespeare_description = get_description(&pokemon_name).await?;

    Ok(HttpResponse::Ok().json(Pokemon {
        name: pokemon_name,
        description: shakespeare_description,
    }))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();

    let host = "0.0.0.0";
    let port = 8080;
    HttpServer::new(|| App::new().service(get_pokemon_shakespeare_description))
        .bind((host, port))?
        .run()
        .await
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix_web::{
        dev::{Body, ResponseBody},
        test, App,
    };

    trait BodyTest {
        fn as_str(&self) -> &str;
    }

    impl BodyTest for ResponseBody<Body> {
        fn as_str(&self) -> &str {
            match self {
                ResponseBody::Body(ref b) => match b {
                    Body::Bytes(ref by) => std::str::from_utf8(&by).unwrap(),
                    _ => panic!(),
                },
                ResponseBody::Other(ref b) => match b {
                    Body::Bytes(ref by) => std::str::from_utf8(&by).unwrap(),
                    _ => panic!(),
                },
            }
        }
    }

    #[tokio::test]
    #[ignore]
    async fn test_get_pokemon_description() {
        let mut app =
            test::init_service(App::new().service(get_pokemon_shakespeare_description)).await;
        let req = test::TestRequest::with_uri("/pokemon/magikarp").to_request();
        let mut resp = test::call_service(&mut app, req).await;

        assert!(resp.status().is_success());
        assert_eq!(
            "application/json",
            resp.headers()
                .get("content-type")
                .unwrap()
                .to_str()
                .unwrap()
        );

        let body = resp.take_body();
        let body_str = body.as_str();
        let actual_magikarp: Pokemon = serde_json::from_str(body_str).unwrap();

        let expected_magikarp = Pokemon {
            name: String::from("magikarp"),
            description: String::from("In the distant past,  't wast somewhat stronger than the horribly weak descendants yond exist the present day."),
        };

        assert_eq!(actual_magikarp, expected_magikarp);
    }
}
