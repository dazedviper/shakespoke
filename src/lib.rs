#![forbid(unsafe_code)]

//! # Shakespoke
//!
//! A library for retrieving Pokémon's descriptions in Shakespearean English.

use async_trait::async_trait;
use log::info;
use shakespeare::ShakespeareResult;
use thiserror::Error;

mod shakespeare;

pub mod error {
    use super::*;

    #[derive(Error, Debug, PartialEq)]
    pub enum Error {
        /// Pokémon description in English not found.
        #[error("Description Not Found")]
        DescriptionNotFound,

        /// Quota to Shakespeare API exceeded. See the [API's
        /// documentation](https://funtranslations.com/api/shakespeare) for ratelimiting quotas.
        #[error("Quota to Shakespeare API exceeded")]
        QuotaError,

        /// Catch-all error for any unexpected errors that may occur.
        /// Note: attempting to retrieve a non-existent Pokémon's description will yield this
        /// error.
        #[error("Unexpected error when calling Shakespeare API or PokéAPI")]
        UnexpectedError,
    }

    impl From<minreq::Error> for Error {
        fn from(_: minreq::Error) -> Error {
            Error::UnexpectedError
        }
    }

    impl From<shakespeare::Error> for Error {
        fn from(error: shakespeare::Error) -> Error {
            match error {
                shakespeare::Error::QuotaError => Error::QuotaError,
                _ => Error::UnexpectedError,
            }
        }
    }
}

fn clean_description(description: &str) -> String {
    description
        .lines()
        .map(|l| l.trim().replace("\u{c}", " "))
        .collect::<Vec<String>>()
        .join(" ")
}

trait PokemonSpecies {
    fn from_name(&self, pokemon_name: &str) -> Result<pokerust::PokemonSpecies, minreq::Error>;
}

struct PokemonSpeciesClient;

impl PokemonSpecies for PokemonSpeciesClient {
    fn from_name(&self, pokemon_name: &str) -> Result<pokerust::PokemonSpecies, minreq::Error> {
        use pokerust::FromName;
        pokerust::PokemonSpecies::from_name(&pokemon_name)
    }
}

#[async_trait]
trait ShakespeareTranslator {
    async fn translate(&self, text: &str) -> ShakespeareResult;
}

struct ShakespeareTranslatorClient;

#[async_trait]
impl ShakespeareTranslator for ShakespeareTranslatorClient {
    async fn translate(&self, text: &str) -> ShakespeareResult {
        shakespeare::translate(&text).await
    }
}

/// Given a Pokémon's name in `pokemon_name`, returns a description of it in Shakespearean English.
pub async fn get_description(pokemon_name: &str) -> Result<String, error::Error> {
    get_description_inner(
        pokemon_name,
        PokemonSpeciesClient {},
        ShakespeareTranslatorClient {},
    )
    .await
}

async fn get_description_inner(
    pokemon_name: &str,
    pokemon_species_client: impl PokemonSpecies,
    shakespeare_translator_client: impl ShakespeareTranslator,
) -> Result<String, error::Error> {
    let pokemon_species = pokemon_species_client.from_name(&pokemon_name)?;

    let first_flavor_text: pokerust::FlavorText = pokemon_species
        .flavor_text_entries
        .into_iter()
        .find(|flavor_text| flavor_text.language.name == "en")
        .ok_or(error::Error::DescriptionNotFound)?;

    let description = clean_description(&first_flavor_text.flavor_text);
    info!("Cleaned original description: {}", &description);
    let shakespeare_description = shakespeare_translator_client
        .translate(&description)
        .await?;

    Ok(shakespeare_description)
}

#[cfg(test)]
mod tests {
    use serde_json::json;
    use std::path::{Path, PathBuf};

    // Grabbed from res/pikachu.json
    const PIKACHU_FLAVOR_TEXT: &str = "When several of these POKéMON gather, their electricity could build and cause lightning storms.";

    const PIKACHU_FLAVOR_TEXT_TRANSLATION: &str = "At which hour several of these pokémon gather,  their electricity couldst buildeth and cause lightning storms.";
    const DEFAULT_TRANSLATION: &str = "To be or not to be";

    use super::*;

    struct ShakespeareTranslatorMockClient;

    fn pikachu_json() -> PathBuf {
        std::env::current_dir()
            .unwrap()
            .join(Path::new("res/pikachu.json"))
    }

    #[async_trait]
    impl ShakespeareTranslator for ShakespeareTranslatorMockClient {
        async fn translate(&self, text: &str) -> ShakespeareResult {
            let translation = match text {
                PIKACHU_FLAVOR_TEXT => PIKACHU_FLAVOR_TEXT_TRANSLATION,
                _ => DEFAULT_TRANSLATION,
            };

            Ok(String::from(translation))
        }
    }

    #[tokio::test]
    async fn unexpected_error_from_pokerust() {
        struct PokemonSpeciesMockClient;

        impl PokemonSpecies for PokemonSpeciesMockClient {
            fn from_name(
                &self,
                _pokemon_name: &str,
            ) -> Result<pokerust::PokemonSpecies, minreq::Error> {
                Err(minreq::Error::Other("other"))
            }
        }

        assert_eq!(
            Err(error::Error::UnexpectedError),
            get_description_inner(
                "david",
                PokemonSpeciesMockClient {},
                ShakespeareTranslatorMockClient {}
            )
            .await
        );
    }

    #[tokio::test]
    async fn pokemon_with_no_english_description() {
        struct PokemonSpeciesMockClient;

        impl PokemonSpecies for PokemonSpeciesMockClient {
            fn from_name(
                &self,
                _pokemon_name: &str,
            ) -> Result<pokerust::PokemonSpecies, minreq::Error> {
                let mut pikachu_body: serde_json::value::Value =
                    serde_json::from_str(&std::fs::read_to_string(pikachu_json()).unwrap())
                        .unwrap();

                *pikachu_body.get_mut("flavor_text_entries").unwrap() = json!([{
                    "flavor_text": "最近發表了聚集大量皮卡丘\n來建造發電廠的計畫。",
                    "language": {
                        "name": "zh-Hant",
                        "url": "https://pokeapi.co/api/v2/language/4/"
                    },
                    "version": {
                        "name": "sun",
                        "url": "https://pokeapi.co/api/v2/version/27/"
                    }
                }]);

                let pikachu: pokerust::PokemonSpecies =
                    serde_json::from_value(pikachu_body).unwrap();

                Ok(pikachu)
            }
        }

        assert_eq!(
            Err(error::Error::DescriptionNotFound),
            get_description_inner(
                "pikachu",
                PokemonSpeciesMockClient {},
                ShakespeareTranslatorMockClient {}
            )
            .await
        );
    }

    #[tokio::test]
    async fn successful_get_description() {
        struct PokemonSpeciesMockClient;

        impl PokemonSpecies for PokemonSpeciesMockClient {
            fn from_name(
                &self,
                _pokemon_name: &str,
            ) -> Result<pokerust::PokemonSpecies, minreq::Error> {
                let pikachu: pokerust::PokemonSpecies =
                    serde_json::from_str(&std::fs::read_to_string(pikachu_json()).unwrap())
                        .unwrap();

                Ok(pikachu)
            }
        }

        assert_eq!(
            Ok(String::from(PIKACHU_FLAVOR_TEXT_TRANSLATION)),
            get_description_inner(
                "pikachu",
                PokemonSpeciesMockClient {},
                ShakespeareTranslatorMockClient {}
            )
            .await
        );
    }
}
