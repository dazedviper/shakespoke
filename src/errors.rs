use actix_web::{HttpResponse, ResponseError};

use log::error;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ServiceError {
    #[error("Internal Server Error")]
    InternalServerError,

    #[error("Description Not Found")]
    DescriptionNotFound,

    #[error("Quota to API exceeded")]
    QuotaError,
}

impl ResponseError for ServiceError {
    fn error_response(&self) -> HttpResponse {
        match self {
            ServiceError::InternalServerError => {
                HttpResponse::InternalServerError().body("Internal Server Error")
            }
            ServiceError::DescriptionNotFound => {
                HttpResponse::NotFound().body("Description Not Found")
            }
            ServiceError::QuotaError => {
                HttpResponse::TooManyRequests().body("Quota to API exceeded")
            }
        }
    }
}

impl From<shakespoke::error::Error> for ServiceError {
    fn from(error: shakespoke::error::Error) -> ServiceError {
        error!("{}", error);

        match error {
            shakespoke::error::Error::DescriptionNotFound => ServiceError::DescriptionNotFound,
            shakespoke::error::Error::QuotaError => ServiceError::QuotaError,
            shakespoke::error::Error::UnexpectedError => ServiceError::InternalServerError,
        }
    }
}
