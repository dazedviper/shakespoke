FROM ekidd/rust-musl-builder:1.49.0 as builder
ADD . ./
RUN sudo chown -R rust:rust /home/rust
RUN cargo build --release

FROM alpine:latest
RUN apk --no-cache add ca-certificates
COPY --from=builder /home/rust/src/target/x86_64-unknown-linux-musl/release/shakespoke /usr/local/bin/shakespoke
EXPOSE 8080
ENV RUST_LOG=info
CMD ["/usr/local/bin/shakespoke"]
